# Automation Documentation

This documentation provides step-by-step instructions on how to set up and automate the provisioning of a fully functional environment using Infrastructure as Code (IaC) concepts, Terraform, and Ansible. The goal is to configure an initial virtual network, public and private subnets, a compute instance, and deploy services using Terraform modules. Additionally, Ansible will be used to automate the configuration of the compute instance, including Elasticsearch setup.

## Prerequisites

Before starting the automation process, ensure you have the following prerequisites in place:

1. An AWS Free Tier account with access key ID and secret access key.

2. A macOS machine with the following tools installed:
   - Terraform (https://www.terraform.io/downloads.html)
   - Ansible (installed via Homebrew with the command `brew install ansible`)
   - AWS CLI (configured with your AWS access key ID and secret access key)

##  Set Up AWS Account

1. Sign up for an AWS Free Tier account if you haven't already. This will provide you with the necessary credentials to interact with AWS services.

2. Obtain your AWS access key ID and secret access key, which will be used to authenticate your AWS API requests.

3. Configure the AWS CLI by running the command `aws configure` in Terminal. Provide your AWS access key ID, secret access key, default region (e.g., `eu-central-1`), and default output format (e.g., `json`).


## Infrastructure Provisioning

Terraform Configuration
The infrastructure provisioning was performed using Terraform. The following steps were executed to create the fully functional environment:

Step 1: Configuration
Created the necessary Terraform configuration files:
provider.tf: Configured the AWS provider and specified the desired region.
variables.tf: Defined the required variables for the infrastructure.
main.tf: Defined the virtual network, public and private subnets, and compute instances.
security_groups.tf: Configured the security groups for the instances.
Step 2: Provisioning
Initialized the Terraform working directory by executing the command: terraform init.
Validated the Terraform configuration by running the command: terraform validate.
Executed the command: terraform plan to review the planned infrastructure changes.
Verified the output of the plan and ensured that it aligns with the desired infrastructure setup.
Applied the changes by executing: terraform apply.
Monitored the provisioning process and reviewed the output for any errors or issues.
Verified the successful creation of the infrastructure resources in the AWS console.


provider.tf:
This file specifies the provider, which in this case is AWS (Amazon Web Services).
It defines the necessary credentials and region for connecting to the AWS services.
It ensures that Terraform communicates with the correct AWS region when creating resources.
variables.tf:
This file defines the variables that are used throughout the Terraform configuration.
Variables allow for parameterization and flexibility in the configuration.
Examples of variables that could be defined include the desired subnet CIDR blocks, instance types, and other configurable parameters.
main.tf:
This file is the main Terraform configuration file that defines the infrastructure resources.
It typically includes the following sections:
provider block: Specifies the provider and its configuration (already defined in provider.tf in this case).
resource blocks: Define the AWS resources that need to be created, such as VPC, subnets, security groups, and compute instances.
data blocks: Retrieve information from existing resources or external sources, such as retrieving the VPC ID or subnet IDs.
output block: Specifies the output values that are useful for other parts of the system or for reference.
security_groups.tf:
This file contains the configuration for the security groups used by the compute instances.
Security groups define the inbound and outbound rules that control the traffic allowed to and from the instances.
It specifies the rules such as open ports, allowed protocols, and IP ranges.
In summary, the Terraform configuration files define the desired infrastructure state and resources using a declarative syntax. The files specify the provider (AWS), variables (configurable parameters), resources (VPC, subnets, security groups, instances), and security group rules.

By executing Terraform commands such as terraform init, terraform plan, and terraform apply, the Terraform engine reads these configuration files, communicates with the provider (AWS), and creates or modifies the infrastructure resources according to the specified configuration.

The separation of configuration into multiple files helps organize the infrastructure definition and allows for easier maintenance and extensibility.



Elasticsearch Configuration

Ansible Configuration
To deploy the Elasticsearch configuration, Ansible was used. The following steps were taken:

Created an Ansible playbook named elasticsearch.yml to configure LVM, add PV, VG, LV, format the volume as ext4, and mount it as a replacement for /var.
Executed the Ansible playbook using the command ansible-playbook -i <inventory_file> elasticsearch.yml.
Verified the successful execution of the Ansible playbook and the desired Elasticsearch configuration.
Mounting /var with /etc/fstab
To ensure persistence and handle situations when the disk is disconnected, the /var directory was mounted using the /etc/fstab file. The following modification was made:

Added the following entry to the /etc/fstab file:
/dev/vg_elasticsearch/lv_elasticsearch /var ext4 defaults 0 0

Acceptance Testing

Acceptance tests were performed to validate the deployed Elasticsearch configuration and ensure the system's functionality. The following tests were executed:

Test 1: Verify that Elasticsearch is running and accessible.
Test 2: Validate that logs are stored in the /var/log mountpoint and are available for the OS, daemons, and root user to read.
Test 3: Perform a basic search operation on Elasticsearch and confirm the expected results.



Git Repository

All the code and configuration files used for infrastructure provisioning, Elasticsearch configuration, and automation are stored in the Git repository. The repository provides version control and serves as a collaborative platform for the project.



## exit9.06

Troubleshooting AWS EC2 Instance Connectivity Issues

Problem Description
I was experiencing issues connecting to an EC2 instance on AWS.

Initial Investigation
Checked the security group settings to ensure that SSH (port 22) was allowed.
Verified that the instance was running and had a public IP address assigned.
Ensured that the key pair used for SSH was correct and had the appropriate permissions.
Troubleshooting Steps
Verified the key pair location and permissions:
Confirmed that the key file (tfkey.pem) was located in the correct directory (~/.ssh/).
Used the chmod command (chmod 400 ~/.ssh/tfkey.pem) to set the correct permissions on the key file.
Verified the SSH connection command:
Used the following SSH command: ssh -i "tfkey.pem" ec2-user@<public-ip>
Checked the instance reachability:
Made sure that the instance was in a subnet with a route to the internet (via a NAT gateway or appropriate network configuration).
Checked the routing table configuration:
Ensured that the routing table had an entry for the subnet's CIDR range (e.g., 10.0.0.0/16) and a route to the internet gateway (0.0.0.0/0).
Verified the availability zone and subnet configuration:
Made sure that the instance was launched in the correct availability zone and associated with the desired subnet.
Corrections and Resolutions
Adjusted the routing table:
Added a route entry for 0.0.0.0/0 to route traffic to the internet gateway.
This allowed the instance in the private subnet to reach the internet.
Moved the key file to the correct directory:
Used the mv command (mv tfkey.pem ~/.ssh/) to move the key file (tfkey.pem) to the ~/.ssh/ directory.
Set the correct permissions on the key file:
Used the chmod command (chmod 400 ~/.ssh/tfkey.pem) to set the correct permissions on the key file.
Updated the SSH connection command:
Used the following SSH command to connect: ssh -i "tfkey.pem" <your-username>@<public-ip>
After these corrections and troubleshooting steps, I was able to successfully connect to the EC2 instance.




11.06:

Elasticsearch Configuration Documentation

This documentation outlines the steps and commands required to configure Elasticsearch on a target compute instance using Ansible and LVM (Logical Volume Manager).

Prerequisites

Ansible is installed on the local machine.
SSH access is properly configured on the target compute instance.
Steps

Create an Ansible playbook (elasticsearch.yml) with the following tasks:
Install LVM packages (lvm2).
Create a physical volume (pvcreate) on the target device.
Create a volume group (vgcreate).
Create a logical volume (lvcreate) with the desired size.
Format the logical volume with the ext4 filesystem (mkfs.ext4).
Mount the logical volume to the /var directory using the mount module in Ansible.
Update the /etc/fstab file to include an entry for the logical volume, ensuring it is mounted persistently.
Execute the Ansible playbook by running the command:
css
Copy code
ansible-playbook -i inventory.ini elasticsearch.yml
The playbook will execute the tasks on the target compute instance, configuring LVM, formatting the volume, and mounting it to /var. It ensures idempotency, meaning it only makes changes if necessary.
After running the playbook, the target compute instance will be configured with Elasticsearch:
LVM will be set up with a physical volume, volume group, and logical volume.
The logical volume will be formatted with the ext4 filesystem.
The logical volume will be mounted to the /var directory.
The /etc/fstab file will be updated to include the entry for the logical volume, ensuring it is mounted persistently.
Please note that this documentation serves as a general guide. Make sure to review the playbook and adjust it as needed for your specific requirements and environment.


ec2-user@ip-10-0-1-13 ~]$ pvdisplay /dev/xvdf
  WARNING: Running as a non-root user. Functionality may be unavailable.
  /run/lock/lvm/P_global:aux: open failed: Permission denied
[ec2-user@ip-10-0-1-13 ~]$ sudo pvdisplay /dev/xvdf
  --- Physical volume ---
  PV Name               /dev/sdf
  VG Name               my_vg
  PV Size               10.00 GiB / not usable 4.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              2559
  Free PE               0
  Allocated PE          2559
  PV UUID               rYmVWf-VTiB-xugt-Og92-HiKg-zDnG-sJYVoF
   
[ec2-user@ip-10-0-1-13 ~]$ sudo vgdisplay my_vg
  --- Volume group ---
  VG Name               my_vg
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  2
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                1
  Open LV               1
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <10.00 GiB
  PE Size               4.00 MiB
  Total PE              2559
  Alloc PE / Size       2559 / <10.00 GiB
  Free  PE / Size       0 / 0   
  VG UUID               9TvCzH-YjhE-nue1-clWL-5JkF-Rtit-YekiDl
   
[ec2-user@ip-10-0-1-13 ~]$ sudo lvdisplay /dev/my_vg/my_lv
  --- Logical volume ---
  LV Path                /dev/my_vg/my_lv
  LV Name                my_lv
  VG Name                my_vg
  LV UUID                h3UX5d-PRTV-fqai-ynCM-wMQh-rVCB-KnbQMG
  LV Write Access        read/write
  LV Creation host, time ip-10-0-1-13.eu-central-1.compute.internal, 2023-06-10 22:44:06 +0000
  LV Status              available
  # open                 1
  LV Size                <10.00 GiB
  Current LE             2559
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:0
   
[ec2-user@ip-10-0-1-13 ~]$ cat /etc/fstab
#
UUID=55a1aebd-f196-4f84-8afe-075f5d1dda63     /           xfs    defaults,noatime  1   1
UUID=0383-1543        /boot/efi       vfat    defaults,noatime,uid=0,gid=0,umask=0077,shortname=winnt,x-systemd.automount 0 2
/dev/my_vg/my_lv /var ext4 defaults 0 0
/dev/my_vg/my_lv   /var   ext4   defaults   0    2
[ec2-user@ip-10-0-1-13 ~]$ systemctl status elasticsearch
Unit elasticsearch.service could not be found.
