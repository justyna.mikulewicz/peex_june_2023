# Configure the AWS provider
provider "aws" {
  region = "eu-central-1"
}

# Create a key pair
resource "aws_key_pair" "peex" {
  key_name   = "tfkey"
  public_key = file("~/.ssh/id_rsa.pub")
}

# Include the networking module
module "networking" {
  source = "./modules/networking"
  
  # Pass the necessary variables to the networking module
  vpc_cidr_block       = "10.0.0.0/16"
  subnet_cidr_block    = "10.0.1.0/24"
}