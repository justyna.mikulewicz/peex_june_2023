# Create a VPC
resource "aws_vpc" "peex" {
  cidr_block = var.vpc_cidr_block
}

resource "aws_internet_gateway" "PexIGW" {
  vpc_id = aws_vpc.peex.id
  tags = {
    Name = "PexIGW"
  }
}

# Create a subnet
resource "aws_subnet" "peex" {
  vpc_id                  = aws_vpc.peex.id
  availability_zone       = "eu-central-1a"
  cidr_block              = var.subnet_cidr_block
}

resource "aws_route_table" "peex" {
  vpc_id = aws_vpc.peex.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.PexIGW.id
  }

  tags = {
    Name = "PeEx_route_table"
  }
}

resource "aws_route_table_association" "peex_association" {
  subnet_id      = aws_subnet.peex.id
  route_table_id = aws_route_table.peex.id
}

resource "aws_security_group" "peex" {
  name        = "peex-sg"
  description = "PeEx security group"
  vpc_id      = aws_vpc.peex.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
